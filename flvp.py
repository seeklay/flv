from ADTS import ADTS
from FLV import FLV
import struct
import time
import sys

file = sys.argv[1]

f = open(file, 'rb')

r = f.read()
f.close()

print(f'Readed size: {len(r)} bytes')

if r[:3] != b"FLV":
    raise Exception('File not FLV!')

if r[3] != 1:
    raise Exception('Unknown FLV format version!')

if r[4] == 1:
    print('File contains only video!')
elif r[4] == 4:
    print('File contains only audio!')
elif r[4] == 5:
    print('File contains audio and video!')
else:
    raise Exception('Unknow FLV header Flags value!')

header_size = struct.unpack('>I', r[5:9])[0]
if header_size != 9:
    raise Exception('Unknow FLV header extensions!')

packets = r[9:]
print(f'Packets len: {len(packets)} bytes')

a = True
b = True

aac_packets = []

def processAudioData(d, c):
    global a
    if a:
        FLV.showAudioData(d)
        a = False    
    aac_packets.append(ADTS.ADTSPacket(ADTS.ADTSHeader(ADTS.AudioType(0),
                                                        ADTS.SampleRate(4),
                                                        ADTS.ChannelConf(2),
                                                        2047,
                                                        1), d.Data.Data))

def processVideoData(d, c):
    global b
    if b:
        FLV.showVideoData(d)
        b = False

packetCounter = 0

def on_pack(p):
    global packetCounter
    if packetCounter == 0:
        if p.flv_tag.Tag_Type != 18:
            raise Exception('First tag must be a AMF metadata!')                    
        metadata, AV = FLV.demuxTag(p.flv_tag) 
        print(metadata.metadata)   
        packetCounter += 1
        return
        

    DATA, AV = FLV.demuxTag(p.flv_tag)
    if AV:
        processAudioData(DATA, packetCounter)
    else:
        processVideoData(DATA, packetCounter)
    
    packetCounter += 1
    print(f'Progress: {packetCounter}', end='\r')    
        

FLV.demuxPackets(packets, on_packet=on_pack)
print(f'done! demuxed packets: {packetCounter}')

print(f'start muxing! out: x.aac')
f = open('x.aac', 'wb')
f.write(ADTS.ADTS.muxPackets(aac_packets))
f.close()