import struct

class AMF:
    @staticmethod
    def readSCRIPTDATASTRING(data):
        string_length = struct.unpack('>H', data[:2])[0]
        string_data = data[2:string_length+2]
        other = data[string_length+2:]
        return string_data.decode(), other
    
    @staticmethod
    def readSCRIPTDATAVALUE(data):
        data_type = data[0]
        if data_type == 0:
            value = struct.unpack('>d', data[1:9])[0]
            other = data[9:]
            return value, other

        elif data_type == 1:
            return data[0], data[2:]
        
        elif data_type == 2:
            data, other = AMF.readSCRIPTDATASTRING(data[1:])
            return data, other
        
        elif data_type == 8:
            out_assoc = {}
            array_len = struct.unpack('>I', data[1:5])[0]
            buff = data[5:]
            while 1:
                key, buff = AMF.readSCRIPTDATASTRING(buff)
                val, buff = AMF.readSCRIPTDATAVALUE(buff)
                out_assoc[key] = val
                if buff == b"\x00\x00\x09":
                    break
            return out_assoc

        else:
            raise Exception(f'Called unrealised AMF script_data_value with id: {data_type}')
    
    @staticmethod
    def readSCRIPTDATAOBJECT(data):
        name, oth = AMF.readSCRIPTDATASTRING(data)
        value = AMF.readSCRIPTDATAVALUE(oth)
        return {name: value}
