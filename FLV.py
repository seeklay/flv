from FLVstct import SoundRate, SoundFormat, SoundSize, SoundType, FrameType, CodecID, AACPacketType, AVCPacketType
from bitstruct import bitstruct
from AMF import AMF
import struct

class FLVPacket:    
    def __init__(self, size_of_pr_tag, flv_tag):
        self.size_of_pr_tag = size_of_pr_tag
        self.flv_tag = flv_tag

class FLVTag:
    def __init__(self,
                Tag_Type,
                DataSize,
                Timestamp,
                TimestampExtended,
                StreamID,
                Data):
        self.Tag_Type = Tag_Type
        self.DataSize = DataSize
        self.Timestamp = Timestamp
        self.TimestampExtended = TimestampExtended
        self.StreamID = StreamID
        self.Data = Data

class AACAUDIODATA:
    def __init__(self, o):
        self.AACPacketType = AACPacketType(o[0])
        self.Data = o[1:]

class AVCVIDEOPACKET:
    def __init__(self, o):
        self.AVCPacketType = AVCPacketType(o[0])      
        self.CompositionTime = o[1:4]
        self.Data = o[4:]

class MetaData:
    def __init__(self, o):
        self.metadata = o['onMetaData']

class AudioData:
    def __init__(self, o):
        b = bitstruct.unpack(o[0])
        self.SoundFormat = SoundFormat(int(b[:4], 2))
        self.SoundRate = SoundRate(int(b[4:6], 2))
        self.SoundSize = SoundSize(int(b[6], 2))
        self.SoundType = SoundType(int(b[7], 2))
        self.Data = o[1:]
        if self.SoundFormat == SoundFormat.AAC:
            self.Data = AACAUDIODATA(self.Data)

class VideoData:
    def __init__(self, o):
        b = bitstruct.unpack(o[0])
        self.FrameType = FrameType(int(b[:4], 2))
        self.CodecID = CodecID(int(b[4:], 2))
        self.Data = o[1:]
        if self.CodecID == CodecID.AVC:
            self.Data = AVCVIDEOPACKET(self.Data)

class FLV:

    @staticmethod
    def demuxPackets(raw_packets, on_packet=None):
        packets = []
        buff = raw_packets
        while 1:
            size_of_pr_tag = struct.unpack('>I', buff[:4])[0]
            Tag_Type = buff[4]
            DataSize = struct.unpack('>I', b"\x00"+buff[5:8])[0]
            Timestamp = struct.unpack('>I', b"\x00"+buff[8:11])[0]
            TimestampExtended = buff[11]
            StreamID = struct.unpack('>I', b"\x00"+buff[12:15])[0]
            Data = buff[15:DataSize+15]        
            buff = buff[DataSize+15:]
            packets.append(FLVPacket(size_of_pr_tag, FLVTag(Tag_Type, DataSize, Timestamp, TimestampExtended, StreamID, Data)))
            if on_packet:
                on_packet(packets[len(packets)-1])
            if len(buff) == 4:
                break
        return packets
    
    @staticmethod
    def demuxTag(tag):
        if tag.Tag_Type == 18:                        
            return MetaData(AMF.readSCRIPTDATAOBJECT(tag.Data[1:])), None
        elif tag.Tag_Type == 8:
            return AudioData(tag.Data), True
        elif tag.Tag_Type == 9:
            return VideoData(tag.Data), False
        else:
            raise Exception(f'Unknown FLV tag type! [{tag.Tag_Type}]')            
    
    @staticmethod
    def showAudioData(d):
        print()
        print(f"SoundFormat: {d.SoundFormat}\nSoundRate: {d.SoundRate}\nSoundSize: {d.SoundSize}\nSoundType: {d.SoundType}")
        if d.SoundFormat == SoundFormat.AAC:
            print(f'AACPacketType: {d.Data.AACPacketType}')
        print()        
    
    @staticmethod
    def showVideoData(d):
        print()
        print(f"FrameType: {d.FrameType}\nCodecID: {d.CodecID}")
        if d.CodecID == CodecID.AVC:
            print(f'AVCPacketType: {d.Data.AVCPacketType}\nComposition time: {d.Data.CompositionTime}')
        print()