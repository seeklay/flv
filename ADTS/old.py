
if r[:2] != b"\xff\xf1":
    raise Exception('File not ADTS!')

has_crc = obit(r[1], Blame5, Blame2, Uint1)[2]
if not has_crc:
    print('ADTS frame has CRC checksum, headers size is 9 bytes!')

atype, asf, _, cu = obit(r[2], Uint2, Uint4, Uint1, Blame1)

print(AudioType(atype), SampleRate(asf))

cl, _, fsu = obit(r[3], Blame2, Uint4, Blame2)
print(ChannelConf(int(cu+cl, 2)))

fsm = obit(r[4], Blame8)[0]

fsl, bfu = obit(r[5], Blame3, Blame5)

#print(fsu, fsm, fsl)
print(f'Frame size: {int(fsu+fsm+fsl, 2)}')

bfl, nof = obit(r[6], Blame6, Uint2)
print(f'Buffer fullness: {int(bfu+bfl, 2)}')
print(f'Number of frames in ADTS frame: {nof+1}')

aac = r[7:]
print(aac[:int(fsu+fsm+fsl, 2)])