from .obit import obit, Int, Export, bito
import enum

class AudioType(enum.Enum):
    Main = 0
    LC = 1
    SSR = 2
    RSV = 3

class SampleRate(enum.Enum):
    k96 = 0
    k88 = 1
    k64 = 2
    k48 = 3
    k44 = 4
    k32 = 5
    k24 = 6
    k22 = 7
    k16 = 8
    k12 = 9
    k11 = 10
    k8 = 11
    k7 = 12
    rsv13 = 13
    rsv14 = 14
    rsv15 = 15

class ChannelConf(enum.Enum):
    a = 0
    mono = 1
    stereo = 2
    d = 3
    e = 4
    f = 5
    g = 6
    h = 7

class ADTSHeader:
    def __init__(self, AudioType, SampleRate, ChannelConf, BuffFullness, NumofFrames):
        self.AudioType = AudioType
        self.SampleRate = SampleRate
        self.ChannelConf = ChannelConf
        self.BuffFullness = BuffFullness
        self.NumofFrames = NumofFrames
    
    def show(self):
        print(f'AudioType: {self.AudioType}')
        print(f'SampleRate: {self.SampleRate}')
        print(f'ChannelConf: {self.ChannelConf}')
        print(f'Bfull: {self.BuffFullness}')
        print(f'Numoff: {self.NumofFrames}')

class ADTSPacket:
    def __init__(self, Header, Data):
        self.Header = Header
        self.Data = Data

class ADTS:
    def demuxPackets(raw_packets, on_packet=None):
        packets = []
        buff = raw_packets
        while 1:            
            if buff[:2] == b"\xff\xf1":
                h_len = 7
            elif buff[:2] == b"\xff\xf0":
                h_len = 9
            else:
                raise Exception('NO no no!!!')       
            a_type, a_sf, _, a_cc_u = obit(buff[2], Int(2), Int(4), Int(1), Export(1))
            a_cc_l, _, p_len_u = obit(buff[3], Export(2), Int(4), Export(2))
            p_len_m = obit(buff[4], Export(8))[0]
            p_len_l, bf_u, = obit(buff[5], Export(3), Export(5))
            bf_l, nof = obit(buff[6], Export(6), Int(2))
            p_len = int(p_len_u+p_len_m+p_len_l, 2)            
            Data = buff[h_len:p_len]            
            buff = buff[p_len:]

            packets.append(ADTSPacket(ADTSHeader(
                AudioType(a_type),
                SampleRate(a_sf),
                ChannelConf(int(a_cc_u+a_cc_l, 2)),
                int(bf_u+bf_l, 2),
                nof+1
            ), Data))            
            if on_packet:
                on_packet(packets[len(packets)-1])                
            if len(buff) == 0:
                break
        return packets

    def muxPackets(packets):
        out = b""
        for p in packets:
            s = '1111111111110001' #sw + mv + la + pa
            s += bin(p.Header.AudioType.value)[2:].rjust(2, '0') # profile
            s += bin(p.Header.SampleRate.value)[2:].rjust(4, '0') #sample rate
            s += '0' #private bit
            s += bin(p.Header.ChannelConf.value)[2:].rjust(3, '0') #channel conf
            s += '0000' # orig+home+cib+cis
            s += bin(len(p.Data)+7)[2:].rjust(13, '0') #len (hlen+plen)
            s += bin(p.Header.BuffFullness)[2:].rjust(11, '0') # buff f-ness
            s += bin(p.Header.NumofFrames-1)[2:].rjust(2, '0') # n of fr
            out += bito(s)
            out += p.Data   
        return out
        

        