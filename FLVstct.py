import enum

class SoundFormat(enum.Enum):
    PCM_PE = 0
    ADPCM = 1
    MP3 = 2
    PCM_LE = 3
    NM16 = 4
    NM8 = 5
    NM = 6
    G711A = 7
    G711MU = 8
    RESERVED = 9
    AAC = 10
    Speex = 11
    MP38 = 14
    DSC = 15

class SoundRate(enum.Enum):
    kHZ_5_5 = 0
    kHz_11 = 1
    kHz_22 = 2
    kHz_44 = 3

class SoundSize(enum.Enum):
    snd8bit = 0
    snd16bit = 1

class SoundType(enum.Enum):
    mono = 0
    stereo = 1

class AACPacketType(enum.Enum):
    aac_seq_header = 0
    aac_raw = 1

class FrameType(enum.Enum):
    keyframe = 1
    interframe = 2
    dinterframe_h263 = 3
    gkeyframe_reserv = 4
    cmd_frame = 5

class CodecID(enum.Enum):
    JPEG = 1
    Sorenson_h263 = 2
    Screen_video = 3
    VP6 = 4
    VP6a = 5
    Screen_video_v2 = 6
    AVC = 7

class AVCPacketType(enum.Enum):
    avc_seq_header = 0
    avc_nalu = 1
    avc_end_of_seq = 2