import struct, time, enum

class FLVPacket:
    def __init__(self, size_of_pr_tag, flv_tag):
        self.size_of_pr_tag = size_of_pr_tag
        self.flv_tag = flv_tag
    
class FLVTag:
    def __init__(self, 
                packet_type,
                payload_size,
                ts_lower,
                ts_upper,
                stream_id,
                payload):
        self.packet_type = packet_type
        self.payload_size = payload_size
        self.ts_lower = ts_lower
        self.ts_upper = ts_upper
        self.stream_id = stream_id
        self.payload = payload       

class FLVAudioCodec(enum.Enum):
    PCM_PE = 0
    ADPCM = 1
    MP3 = 2
    PCM_LE = 3
    AAC = 10

class FLVSoundRate(enum.Enum):
    kHz5_5 = 0
    kHz11 = 1
    kHz22 = 2
    kHz44 = 3

class FLVVcodecID(enum.Enum):
    jpeg = 1
    h263 = 2
    screen_video = 3
    vp6 = 4
    vp6a = 5
    screen_video_v2 = 6
    avc = 7

class FLVFrameType(enum.Enum):
    keyframe = 1
    interframe = 2
    h263_dframe = 3
    gen_kframe = 4
    command_frame = 5

class FLV:

    @staticmethod
    def showPacketInfo(pack):
        print()
        print(f'///PACKET')
        print(f'0. Size of previous FLV TAG (TAG HEADER[11] + PAYLOAD (without packet header)): {pack.size_of_pr_tag}')
        print(f'///FLV TAG')
        print(f'0. Tag type: {pack.flv_tag.packet_type}')
        print(f'1. Payload size: {pack.flv_tag.payload_size}')
        print(f'2. ts_lower: {pack.flv_tag.ts_lower}')
        print(f'3. ts_upper: {pack.flv_tag.ts_upper}')
        print(f'4. stream_id: {pack.flv_tag.stream_id}')
        print(f'5. payload size: {len(pack.flv_tag.payload)}')
        print(f'///FLV TAG\n///PACKET')


    @staticmethod
    def parseAPacket(obj):
        size_of_pr_tag = struct.unpack('>I', obj[:4])[0]
        packet_type = obj[4]
        payload_size = struct.unpack('>I', b"\x00"+obj[5:8])[0]
        ts_lower = struct.unpack('>I', b"\x00"+obj[8:11])[0]
        ts_upper = obj[11]
        stream_id = struct.unpack('>I', b"\x00"+obj[12:15])[0]
        payload = obj[15:payload_size+15]        
        new = obj[payload_size+15:]

        tag = FLVTag(packet_type,
                    payload_size,
                    ts_lower,
                    ts_upper,
                    stream_id,
                    payload)

        pack = FLVPacket(size_of_pr_tag, tag)
        return pack, new
    
    @staticmethod    
    def parse(packets):
        out = []
        buff = packets
        print('Started parsing FLV body')
        c = 0
        while 1:
            pack, buff = FLV.parseAPacket(buff)
            out.append(pack)            
            if len(buff) == 4:                
                break       
            print(f'Progress: {c}', end='\r')
            c += 1
        return out  
