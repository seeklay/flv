import struct

class AMF:
    @staticmethod
    def readSDS(o):
        l = struct.unpack('>H', o[:2])[0]        
        d = o[2:l+2]
        n = o[l+2:]
        return d.decode(), n

    @staticmethod
    def readSDV(o):
        t = o[0]
        if t == 8:
            out = {}
            al = struct.unpack('>I', o[1:5])[0]
            n = o[5:]
            while 1:
                k, n = AMF.readSDS(n)                      
                v, n = AMF.readSDV(n)            
                out[k] = v
                if n == b"\x00\x00\x09":
                    break
            return out
                              
        
        elif t == 0:
            d = struct.unpack('>d', o[1:9])[0]
            n = o[9:]
            return d, n

        elif t == 1:
            return o[1], o[2:]
        elif t == 2:
            d, n = AMF.readSDS(o[1:])
            return d, n
        else:
            print(t)
            exit()


    @staticmethod
    def readSDO(o):
        name, n = AMF.readSDS(o)
        val = AMF.readSDV(n)
        return {name: val}

    @staticmethod
    def parseMeta(p):                
        p = p[1:]        
        return AMF.readSDO(p)


        