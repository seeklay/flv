class bitstruct:          
    def unpack(v):
        x = bin(v)[2:].rjust(8, '0')        
        z = []
        for y in x:
            z.append(bool(int(y)))
        return x